import express from 'express';
import fs from 'fs';
import {promisify} from 'util';

const router = express.Router();
const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);

//Criar a conta
router.post('/',async(req,res)=> {

    try {
     
        let account = req.body;   
        const data = JSON.parse(await readFile(global.fileName, 'utf8') );

        account = {Id: data.nextId++, ...account};

        data.accounts.push(account);

        await writeFile(global.fileName, JSON.stringify(data));

        res.end();

    } catch (err) {
        res.status(400).send({error: err.message});
    }

});


router.put('/Deposito',async(req,res)=> {

    try {
     
        let newSaldo = req.body;   
        const data = JSON.parse(await readFile(global.fileName, 'utf8') );

        let oldAccountIndex = data.accounts.findIndex(account => account.id === newSaldo.id);

        data.accounts[oldAccountIndex].balance += newSaldo.balance;        

        await writeFile(global.fileName, JSON.stringify(data));

        res.end();

    } catch (err) {
        res.status(400).send({error: err.message});
    }


});

router.put('/Saque',async(req,res)=> {

    try {
     
        let newSaldo = req.body;   
        const data = JSON.parse(await readFile(global.fileName, 'utf8') );

        let oldAccountIndex = data.accounts.findIndex(account => account.id === newSaldo.id);

        data.accounts[oldAccountIndex].balance -= newSaldo.balance;        

        await writeFile(global.fileName, JSON.stringify(data));

        res.end();

    } catch (err) {
        res.status(400).send({error: err.message});
    }


});

router.get('/:id',async(req,res)=> {

    try {
     
        const data = JSON.parse(await readFile(global.fileName, 'utf8') );

        const account = data.accounts.filter(account => account.id === parseInt(req.params.id,10));

        if (account){
            res.send(account);
        }else{
            res.end();
        }
        

    } catch (err) {
        res.status(400).send({error: err.message});
    }

});

router.delete('/:id',async(req,res)=> {

    try {
     
        const data = JSON.parse(await readFile(global.fileName, 'utf8') );

        data.accounts = data.accounts.filter(account => account.id !== parseInt(req.params.id,10));

        await writeFile(global.fileName, JSON.stringify(data));

        res.end();

    } catch (err) {
        res.status(400).send({error: err.message});
    }

});


export default router;