import express from 'express';
import fs from 'fs';
import {promisify} from 'util';
import accountRoute from './routes/account.js';

const app = express();

const port = 3005;
const exists = promisify(fs.exists);
const writeFile = promisify(fs.writeFile);

global.fileName = 'accounts.json';

app.use(express.json());
app.use('/account',accountRoute);

app.get('/testeParam/:id', (req,res) => 
              res.send('hello worl6d')
       );

app.listen(port, async()=>{
       
       console.log(global.fileName);

       try {
              const fileExists = await exists(global.fileName);
        
        if (!fileExists){
              const initialJson = { nextId: 1, accounts:[]}
              await writeFile(global.fileName, JSON.stringify(initialJson));             
        };
       

       } catch (error) {
              
       }    
});